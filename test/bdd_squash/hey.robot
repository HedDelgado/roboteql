#C:\Users\Formation\AppData\Local\rasjani\WebDriverManager\bin\geckodriver.exe

*** Settings ***
Library    SeleniumLibrary
Resource    ../../resources/jpetstore.resource

*** Test Cases ***

Connection
    
    #Open Browser and go to front page jpetstore
    Ouvrir Page
   

    #Go to login page
    Go LoginPage
    
    #Put login and password
    PutLogin
    PutPassword
    ClickLogin
    
    #check connection is on
    CheckLoginDone

    #Close Browser    
    CloseBrowser
    
