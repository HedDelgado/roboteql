*** Settings ***
Documentation    Robot
Metadata         ID                           10
Metadata         Automation priority          null
Metadata         Test case importance         Low
Resource         squash_resources.resource
Library          squash_tf.TFParamService
Test Setup       Test Setup
Test Teardown    Test Teardown


*** Test Cases ***
Robot
    [Documentation]    Robot

    &{dataset} =    Retrieve Dataset

    Given Je suis un utilisateur pas connectée à son profil sur la page d'accueil jpetstore
    And Que j'ai cliqué sur le menu 'Sign in'
    And Que je suis arrivé sur la âge d'authentification
    When Je rentre mon"${dataset}[pseudo_utilisateur]"
    And Je rentre mon "${dataset}[mdp]"
    And Je clique sur le bouton 'Login'
    Then Mon authentification réussie et je vois le menu 'Sing Out' apparaître à la place de 'Sign In'
    And Je suis de retour sur la page d'accueil
    And Le message Welcome "${dataset}[pseudo]" apparait à gauche au dessus des menus des espèces animales


*** Keywords ***
Test Setup
    [Documentation]    test setup
    ...                You can define the ${TEST_SETUP} variable with a keyword for setting up all your tests.
    ...                You can define the ${TEST_10_SETUP} variable with a keyword for setting up this specific test.
    ...                If both are defined, ${TEST_10_SETUP} will be run after ${TEST_SETUP}.

    ${TEST_SETUP_VALUE} =       Get Variable Value    ${TEST_SETUP}
    ${TEST_10_SETUP_VALUE} =    Get Variable Value    ${TEST_10_SETUP}
    IF    $TEST_SETUP_VALUE is not None
        Run Keyword    ${TEST_SETUP}
    END
    IF    $TEST_10_SETUP_VALUE is not None
        Run Keyword    ${TEST_10_SETUP}
    END

Test Teardown
    [Documentation]    test teardown
    ...                You can define the ${TEST_TEARDOWN} variable with a keyword for tearing down all your tests.
    ...                You can define the ${TEST_10_TEARDOWN} variable with a keyword for tearing down this specific test.
    ...                If both are defined, ${TEST_TEARDOWN} will be run after ${TEST_10_TEARDOWN}.

    ${TEST_10_TEARDOWN_VALUE} =    Get Variable Value    ${TEST_10_TEARDOWN}
    ${TEST_TEARDOWN_VALUE} =       Get Variable Value    ${TEST_TEARDOWN}
    IF    $TEST_10_TEARDOWN_VALUE is not None
        Run Keyword    ${TEST_10_TEARDOWN}
    END
    IF    $TEST_TEARDOWN_VALUE is not None
        Run Keyword    ${TEST_TEARDOWN}
    END

Retrieve Dataset
    [Documentation]    Retrieves Squash TM's datasets and stores them in a dictionary.
    ...
    ...                For instance, datasets containing 3 parameters "city", "country" and "currency"
    ...                have been defined in Squash TM.
    ...
    ...                First, this keyword retrieves parameter values from Squash TM
    ...                and stores them into variables, using the keyword 'Get Test Param':
    ...                ${city} =    Get Test Param    DS_city
    ...
    ...                Then, this keyword stores the parameters into the &{dataset} dictionary
    ...                with each parameter name as key, and each parameter value as value:
    ...                &{dataset} =    Create Dictionary    city=${city}    country=${country}    currency=${currency}

    ${pseudo_utilisateur} =    Get Test Param    DS_pseudo_utilisateur
    ${mdp} =                   Get Test Param    DS_mdp
    ${pseudo} =                Get Test Param    DS_pseudo

    &{dataset} =    Create Dictionary    pseudo_utilisateur=${pseudo_utilisateur}    mdp=${mdp}    pseudo=${pseudo}

    RETURN    &{dataset}
